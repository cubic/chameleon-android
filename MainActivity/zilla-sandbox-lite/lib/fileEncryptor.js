module.exports = Encryptor; //cmd

function Encryptor() {} // 构造函数模式+原型模式

Encryptor.prototype.crypto = require('crypto');
Encryptor.prototype.fs = require('fs');

Encryptor.prototype.defaultOptions = {
	algorithm: 'aes-128-ecb' // aes192
};

/**
 * [combineOptions description] 配置适配器
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
Encryptor.prototype.combineOptions = function(options) {
	var result = {};
	var defaultOptions = this.defaultOptions;

	for (var key in defaultOptions) {
		//console.log('combineOptions:'+key);
		result[key] = defaultOptions[key];
	}

	for (var key in options) {
		result[key] = options[key];
	}

	return result;
};

/**
 * [encrypt description] 加密文件，A -> B
 * @param  {[type]}   inputPath  [description]
 * @param  {[type]}   outputPath [description]
 * @param  {[type]}   key        [description]
 * @param  {[type]}   options    [description]
 * @param  {Function} callback   [description]
 * @return {[type]}              [description]
 */
Encryptor.prototype.encrypt = function(infile, outfile, key, options, callback) {

	// console.log(inputPath + '+'+ key + '+'+ options + '+'+ callback);

	if (typeof options === 'function') {
		callback = options;
		options = {};
	}

	var fs = this.fs;

	options = this.combineOptions(options);

	var inputStream = fs.createReadStream(infile, {
		flags : 'r',
		encoding : null,
		fd: null,
		mode : 0666,
		highWaterMark: 64 * 1024
	});
	var outputStream = fs.createWriteStream(outfile);

	var keyBuf = new Buffer(key);
	//console.log('comeontom:'+keyBuf);
	var cipher = this.crypto.createCipher(options.algorithm, keyBuf);

	inputStream.on("error", function(err) {
		callback(err);
	});

	inputStream.on('data', function(data) {
		var buf = new Buffer(cipher.update(data), 'binary');
		outputStream.write(buf);
	});

	inputStream.on('end', function() {
		var buf = new Buffer(cipher.final('binary'), 'binary');
		console.log('\t正在加密:' + infile);
		outputStream.write(buf);
		outputStream.end();
		outputStream.on('close', function() {
			// // 删除类型为.*New的文件
			// fs.unlink(inputPath, function() {
			//   fs.rename(inputPath + 'New', inputPath, callback);
			// });
			// // callback();
		});
	});

};

/**
 * [decrypt description] 解密文件 A -> B
 * @param  {[type]}   inputPath  [description]
 * @param  {[type]}   outputPath [description]
 * @param  {[type]}   key        [description]
 * @param  {[type]}   options    [description]
 * @param  {Function} callback   [description]
 * @return {[type]}              [description]
 */
Encryptor.prototype.decrypt = function(infile, outfile, key, options, callback) {

	if (typeof options === 'function') {
		callback = options;
		options = {};
	}

	var fs = this.fs;
	options = this.combineOptions(options);

	var inputStream = fs.createReadStream(infile);
	var outputStream = fs.createWriteStream(outfile);

	var keyBuf = new Buffer(key);
	var cipher = this.crypto.createDecipher(options.algorithm, keyBuf);

	inputStream.on('data', function(data) {
		var buf = new Buffer(cipher.update(data), 'binary');
		outputStream.write(buf);
	});

	inputStream.on('end', function() {
		var buf = new Buffer(cipher.final('binary'), 'binary');
		console.log('\t正在解密:' + infile);
		outputStream.write(buf);
		outputStream.end();
		outputStream.on('close', function() {
			// // 删除类型为.*New的文件
			// fs.unlink(inputPath, function() {
			//   fs.rename(inputPath + 'New', inputPath, callback);
			// });
			// // callback();
		});
	});
};