package com.example.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.testacitvity.R;
import com.foss.DeviceInfoUtil;


public class TestFragment extends Fragment{
	TextView textview;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view  = inflater.inflate(R.layout.fragment_firstss, container, false);
		textview = (TextView) view.findViewById(R.id.textViewPushMessage);
		
		initData();
		return view;
	}
	
	private void initData(){
		String[] value = null ;
		Intent i = getActivity().getIntent();
		if(DeviceInfoUtil.isPad()){
			
			Log.i("", "budle ==================== getArguments() "+getArguments() );
			if(getArguments() != null){
				value = i.getStringArrayExtra("parameters");
			}
			Log.i("", "budle ==================== value "+value);
		}else{
			if(i!=null){
				value = i.getStringArrayExtra("parameters");
			}
		}
		if(value!=null){
			for(int j=0;j<value.length;j++) {
				textview.append("参数"+j+":"+value[j]+"  ");
			}
		}
	}
}
