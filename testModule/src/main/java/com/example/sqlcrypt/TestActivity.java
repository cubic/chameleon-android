package com.example.sqlcrypt;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import com.example.testacitvity.R;
import net.sqlcipher.database.SQLiteDatabase;

import java.util.List;

public class TestActivity extends Activity {

    private Context mCtx;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sql_crypt);
        mCtx = this;

        //
        SQLiteDatabase.loadLibs(mCtx);
        testSqlChiper();
    }

    private void testSqlChiper() {
        findViewById(R.id.btn_db_crypt).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                DaoStu.createTableIfNotExists(mCtx);

                // insert
                BeanStu stu = BeanStu.genRdm();
                System.out.println(stu.toString());

                DaoStu.insert(mCtx, stu);

                // query all
                List<BeanStu> stuList = DaoStu.queryAll(mCtx);
                for(BeanStu aStu : stuList) {
                    Log.i("db crypt", aStu.toString());
                }
            }
        });
    }
}
