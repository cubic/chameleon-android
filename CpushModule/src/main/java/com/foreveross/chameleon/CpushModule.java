package com.foreveross.chameleon;

import android.content.Context;
import android.content.Intent;
import com.foreveross.bsl.util.Preferences;
import com.foreveross.chameleonsdk.CModule;
import com.foreveross.push.socket.PushSetup;
import com.foreveross.push.socket.ServicePush;
import com.foss.AppLog;

public class CpushModule extends CModule {
    private static CpushModule cpushModule;
    private Context context;
    private Intent intent;
    public static CpushModule getCpushModule() {
        return cpushModule;
    }

    @Override
    public void onCreate(CModule module) {
        cpushModule = (CpushModule) module;
        this.context = getcApplication().getmContext();
    }

    public void startService() {
        if(intent == null) {
            intent = new Intent(context, ServicePush.class);
        }
        context.startService(intent);
    }

    public void stopService() {
        if(intent != null) {
            context.stopService(intent);
        }
    }

    @Override
    public void onExit(CModule cModule) {
        super.onExit(cModule);
        // TODO 以guest 连接push 服务器
        String userName = PushSetup.getInstance().getUserName();
        if(!"guest".equals(userName)) {
            Preferences.saveCurrentUserName("guest");
            PushSetup.getInstance().startSetup("Cpush退出，要求以guest 身份连接push服务器", false, false);
        } else {
            AppLog.i("CpushModule exit, 已用guest 登录");
        }
    }
}
