package com.foreveross.push.socket;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.foreveross.bsl.util.Preferences;
import com.foreveross.chameleon.CpushModule;
import com.foss.AppLog;
import com.foss.GeneralUtils;

public class ReceiverNetwork extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean isNetAvail = GeneralUtils.isNetworkConnected();
        //AppLog.i("Network is changed, avail = " + isNetAvail);
            if(Preferences.getPushPluginLaunch()) {
                if (isNetAvail) {
                    context.startService(new Intent(context, ServicePush.class));
                } else {
                    context.stopService(new Intent(context, ServicePush.class));
                }
            }

    }
}
