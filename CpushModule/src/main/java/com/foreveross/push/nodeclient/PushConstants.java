package com.foreveross.push.nodeclient;

/** 
 * @author 黄黎  
 * @version 创建时间：2014年5月13日 下午5:09:03 
 * 类说明 
 */
public class PushConstants {
	public static final String SEND_HEARTBEAT="send.heartbeat";
	public static final String EXPIRED_TIME="expiredTime";
	public static final String PUSH_TOKEN="pushToken";
	public static final String TOKEN_CREATE_TIME="tokenCreateTime";
	public static final String TEMP_HOST="tempHost";
	public static final String TEMP_PORT="tempPort";
    public static final String LOG_TAG="PushService";

    public static final int ID_MESSAGE_NOTIFICATION=121;
}
