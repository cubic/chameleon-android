package com.foreveross.chameleonsdk.config;


import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;
import com.foreveross.bsl.util.PropertiesUtil;


public class URL {
    public static boolean IS_CRYPT_AND_PROVIDER = true;
    private static final String SCHEME_FILE = "file://";
    public static final String SCHEME_PROVIDER = "content://com.foreveross.crypt.WebFileProvider";
    private static final String SCHEME_SETTED = IS_CRYPT_AND_PROVIDER ? SCHEME_PROVIDER : SCHEME_FILE;
    public static final String SD_ROOT = Environment.getExternalStorageDirectory() + "/com.foreveross.chameleon/www";

    public static String BASE_PATH_IN_SD = null;
    public static String WWW_FOLDER_IN_SD = null;
    //cube.properties
//    public static String ANNOUNCE = null;
//    public static String BASE_WEB = null;
//    public static String MUC_BASE = null;
    public static String BASE_WS = null;

    //cube.properties
    public static String PAD_MAIN_URL = null;
    public static String PAD_LOGIN_URL = null;
    public static String PHONE_MAIN_URL = null;
    public static String PHONE_LOGIN_URL = null;


//    public static String UPLOAD_URL = null;
//    public static String SYNC = null;
//    public static String UPLOAD = null;
    public static String LOGIN = null;
    public static String LOGOUT = null;
    public static String UPDATE = null;
    public static String SNAPSHOT = null;
    public static String PUSH_BASE_URL = null;
    public static String CHECKIN_URL = null;
//    public static String CHECKOUT_URL = null;
    public static String FEEDBACK_URL = null;
    public static String AUTH = null;
    //
    public static String UPDATE_RECORD = null;
    public static String GETPUSHMESSAGE = null;

    public static String APP_VERSION = null;
    public static int APP_BUILD = 0;
    public static String APP_PACKAGENAME = null;
    public static String APPKEY = null;

    public static void init(Application context) {
        PropertiesUtil propertiesUtil = PropertiesUtil.readProperties(context, CubeConstants.CUBE_CONFIG);
        APPKEY = propertiesUtil.getString("appKey", "");
//        ANNOUNCE = propertiesUtil.getString("ANNOUNCE", "");
//        String BASE_WEB = propertiesUtil.getString("BASE_WEB", "");
//        UPLOAD_URL = BASE_WEB + "mam/attachment/clientUpload";

//        MUC_BASE = propertiesUtil.getString("MUC_BASE", "");
        BASE_WS = propertiesUtil.getString("BASE_WS", "");

//        SYNC = BASE_WS + "mam/api/mam/clients/android/";
//        UPLOAD = BASE_WS + "mam/api/mam/attachment/upload";
        LOGIN = BASE_WS + "system/api/system/mobile/accounts/login";
        LOGOUT = BASE_WS + "system/api/system/mobile/accounts/logout";
        UPDATE = BASE_WS + "mam/api/mam/clients/update/android";
        UPDATE_RECORD = BASE_WS + "mam/api/mam/clients/update/appcount/android/";
        SNAPSHOT = BASE_WS + "mam/api/mam/clients/widget/";
        PUSH_BASE_URL = BASE_WS + "push/api/";
        GETPUSHMESSAGE = PUSH_BASE_URL + "push-msgs/none-receipts/";
        CHECKIN_URL = PUSH_BASE_URL + "checkinservice/checkins";
//        CHECKOUT_URL = PUSH_BASE_URL + "checkinservice/checkout";
        FEEDBACK_URL = PUSH_BASE_URL + "receipts";

        APP_PACKAGENAME = context.getPackageName();
        BASE_PATH_IN_SD = Environment.getExternalStorageDirectory().getPath() + "/" + APP_PACKAGENAME;
        WWW_FOLDER_IN_SD = BASE_PATH_IN_SD + "/www";

        APP_VERSION = getAppVersion(context);
        APP_BUILD = getAppVersionCode(context);
        AUTH = BASE_WS + "mam/api/mam/clients/apps/android/" + APP_PACKAGENAME + "/" + APP_VERSION + "/validate";

        // 请在cube.properties中配置
        PAD_MAIN_URL = SCHEME_SETTED + getPackagePath(context) + "/www/" + propertiesUtil
                .getString("PAD_MAIN_URL", "");
        PAD_LOGIN_URL = SCHEME_SETTED + getPackagePath(context) + "/www/" + propertiesUtil
                .getString("PAD_LOGIN_URL", "");
        PHONE_MAIN_URL = SCHEME_SETTED + getPackagePath(context) + "/www/" + propertiesUtil
                .getString("PHONE_MAIN_URL", "");
        PHONE_LOGIN_URL = SCHEME_SETTED + getPackagePath(context) + "/www/" + propertiesUtil
                .getString("PHONE_LOGIN_URL", "");
    }

    public static String getDownloadUrl(Context context, String bundle) {
        String DOWNLOAD = BASE_WS + "mam/api/mam/clients/files/";
        return DOWNLOAD + bundle + "?" + "appKey=" + APPKEY;
    }

    public static String getUpdateAppplicationUrl(Context context, String bundle) {
        String DOWNLOAD = BASE_WS + "mam/api/mam/clients/files/";
        return DOWNLOAD + bundle + "?appKey=" + APPKEY;
    }


//    public static String getSessionKey() {
//        String sessionKey = "";
//        return sessionKey;
//    }

    public static String getSdPath(Context context, String identifier) {
        String path = Environment.getExternalStorageDirectory().getPath() + "/" + context.getPackageName();
        String url = path + "/www/" + identifier;

        if (IS_CRYPT_AND_PROVIDER) {
            url = SCHEME_PROVIDER + url;
        }
        System.out.println("getSdPath url=" + url);
        return url;
    }

    public static String getAppVersion(Context context) {
        PackageManager pm = context.getPackageManager();
        PackageInfo pi;
        try {
            pi = pm.getPackageInfo(context.getPackageName(), 0);
            return pi.versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static int getAppVersionCode(Context context) {
        PackageManager pm = context.getPackageManager();
        PackageInfo pi;
        try {
            pi = pm.getPackageInfo(context.getPackageName(), 0);
            return pi.versionCode;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getPackagePath(Context context) {
        String path = Environment.getExternalStorageDirectory().getPath() + "/" + context.getPackageName();
        return path;
    }
}
