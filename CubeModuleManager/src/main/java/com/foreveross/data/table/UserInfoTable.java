package com.foreveross.data.table;

/**
 * 用户信息相关表
 * 
 * @author kuanghaojun
 * 
 */
public class UserInfoTable {
	/**
	 * 表名
	 */
	public static final String TABLENAME = "userifno";

	/**
	 * 表字段: 用户名
	 */
	public static final String USERNAME = "username";
	
	public static final String PASSWORD = "password";
	
	public static final String USERINFOTABLESQL = "create table userinfo " + "("
			+ "username text, "  + "password text " + ")";
}
