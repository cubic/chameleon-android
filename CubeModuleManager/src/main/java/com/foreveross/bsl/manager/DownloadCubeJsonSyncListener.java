package com.foreveross.bsl.manager;

public interface DownloadCubeJsonSyncListener {

	public void downloadStart();
	
	public void downloadFinish();
	
	public void downloadFail();
}
